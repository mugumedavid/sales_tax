﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SalesTax.DataAccessService;

namespace SalesTax.DataContext
{
    public static class SessionManager
    {
        private static IDataAccess context;

        /// <summary>
        /// returns an instance of the data context object
        /// </summary>
        public static IDataAccess Context
        {
            get { return context; }
        }

        /// <summary>
        /// constructor of the static class, will always run first before the Context is retrieved 
        /// It initialises the context, this initilisation can be injected at runtime
        /// </summary>
        static SessionManager()
        {
            context = new SalesTaxDataContext();
        }
    }
}
