﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SalesTax.DataAccessService;
using SalesTax.Model;
using System.Xml.Serialization;
using System.IO;
using SalesTax.Model.Exceptions;

namespace SalesTax.DataContext
{
    /// <summary>
    /// This data context implements the IDataAccess interface
    /// This context (that uses XML files as a database) can be replaced with other contexts implementing other database engines
    /// without altering anything in the upper layers.
    /// </summary>
    public class SalesTaxDataContext: IDataAccess
    {
        private string db_location;

        /// <summary>
        /// default contstructor
        /// database files are in the application's base directory
        /// </summary>
        public SalesTaxDataContext()
        {
            db_location = AppDomain.CurrentDomain.BaseDirectory;
        }

        /// <summary>
        /// with this constructor, we can have our database files in another location
        /// </summary>
        /// <param name="db_location">call it a connection string of sorts</param>
        public SalesTaxDataContext(string db_location)
        {
            this.db_location = db_location;
        }

        /// <summary>
        /// persists a product object to database whilst keeping in mind to update if such a product exists
        /// </summary>
        /// <param name="product">the product object to persist</param>
        public void SaveOrUpdateProduct(Product product)
        {
            //get all items in db
            IList<Product> products = GetAllProducts();

            //trying to implement a SaveOrUpdate by deleting original object and replacing with current
            Product prd = (from p in products where p.Id == product.Id select p).FirstOrDefault<Product>();
            if (prd != null)
                products.Remove(prd);

            //maintain our database rule
            //id==0 means a new item
            //but first, predicates throw and error on empty collections
            if (products.Count > 0)
            {
                if (product.Id == 0)
                    product.Id = products.Max<Product>(p => p.Id) + 1;
            }
            else
            {
                if (product.Id == 0)
                    product.Id = 1;
            }

            products.Add(product);

            //serialize and write to database file
            XmlSerializer serializer = new XmlSerializer(typeof(List<Product>));
            TextWriter tw = new StreamWriter(db_location + "Products.xml");
            serializer.Serialize(tw, products);
            tw.Close();
        }

        /// <summary>
        //  checks if database file exists, then deserialises the contents into a list of products
        /// </summary>
        /// <returns>list of products if they exist or an empty list if they dont exist</returns>
        public IList<Product> GetAllProducts()
        {
            try
            {
                if (File.Exists(db_location + "Products.xml"))
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(List<Product>));
                    TextReader reader = new StreamReader(db_location + "Products.xml");
                    IList<Product> b = (List<Product>)serializer.Deserialize(reader);
                    reader.Close();
                    return b;
                }
                else
                    return new List<Product>();
            }
            catch (Exception ex)
            {
                throw new SalesTaxException("error reading database", ex);
            }
        }

        /// <summary>
        /// persists a tax object to database whilst keeping in mind to update if such a tax exists
        /// </summary>
        /// <param name="product">the tax object to persist</param>
        public void SaveOrUpdateTax(Tax tax)
        {
            IList<Tax> taxes = GetAllTax();

            //else implement a SaveOrUpdate by deleting original object and replacing with current
            Tax tx = (from p in taxes where p.Id == tax.Id select p).FirstOrDefault<Tax>();
            if (tx != null)
                taxes.Remove(tx);

            //maintain our database rule
            //id==0 means a new item
            //but first, predicates throw and error on empty collections
            if (taxes.Count > 0)
            {
                if (tax.Id == 0)
                    tax.Id = taxes.Max<Tax>(p => p.Id) + 1;
            }
            else
            {
                if (tax.Id == 0)
                    tax.Id = 1;
            }
            taxes.Add(tax);

            try
            {
                //serialize and save to database file
                XmlSerializer serializer = new XmlSerializer(typeof(List<Tax>));
                TextWriter tw = new StreamWriter(db_location + "Tax.xml");
                serializer.Serialize(tw, taxes);
                tw.Close();
            }
            catch (Exception ex)
            {
                throw new SalesTaxException("error inserting into database", ex);
            }
        }

        /// <summary>
        /// persists a shopping basket object to database whilst keeping in mind to update if such exists
        /// </summary>
        /// <param name="product">the product object to persist</param>
        public void SaveShoppingCart(ShoppingCart cart)
        {
            IList<ShoppingCart> baskets = GetAllShoppingCarts();

            //trying to implement a SaveOrUpdate by deleting original object and replacing with current
            ShoppingCart prd = (from p in baskets where p.Id == cart.Id select p).FirstOrDefault<ShoppingCart>();
            if (prd != null)
                baskets.Remove(prd);

            //maintain our database rule
            //id==0 means a new item
            //but first, predicates throw and error on empty collections
            if (baskets.Count > 0)
            {
                if (cart.Id == 0)
                    cart.Id = baskets.Max<ShoppingCart>(p => p.Id) + 1;
            }
            else
            {
                if (cart.Id == 0)
                    cart.Id = 1;
            }

            baskets.Add(cart);

            try
            {
                XmlSerializer serializer = new XmlSerializer(typeof(List<ShoppingCart>));
                TextWriter tw = new StreamWriter(db_location + "ShoppingBaskets.xml");
                serializer.Serialize(tw, baskets);
                tw.Close();
            }
            catch (Exception ex)
            {
                throw new SalesTaxException("error inserting into database", ex);
            }
        }

        /// <summary>
        /// retrives all texation objects
        /// </summary>
        /// <returns></returns>
        public IList<Tax> GetAllTax()
        {
            try
            {
                if (File.Exists(db_location + "Tax.xml"))
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(List<Tax>));
                    TextReader reader = new StreamReader(db_location + "Tax.xml");
                    IList<Tax> b = (List<Tax>)serializer.Deserialize(reader);
                    reader.Close();
                    return b;
                }
                else
                    return new List<Tax>();
            }
            catch (Exception ex)
            {
                throw new SalesTaxException("error reading database", ex);
            }
        }

        /// <summary>
        /// retrieves all shopping baskets
        /// </summary>
        /// <returns></returns>
        public IList<ShoppingCart> GetAllShoppingCarts()
        {
            try
            {
                if (File.Exists(db_location + "ShoppingBaskets.xml"))
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(List<ShoppingCart>));
                    TextReader reader = new StreamReader(db_location + "ShoppingBaskets.xml");
                    IList<ShoppingCart> b = (List<ShoppingCart>)serializer.Deserialize(reader);
                    reader.Close();
                    return b;
                }
                else
                    return new List<ShoppingCart>();
            }
            catch (Exception ex)
            {
                throw new SalesTaxException("error reading database", ex);
            }
        }
        
        /// <summary>
        /// retrieves a shopping basket based on its id
        /// </summary>
        /// <param name="id">the Id of the shopping basket to retrieve</param>
        /// <returns>list of persisted shopping baskets</returns>
        public ShoppingCart GetShoppingCartById(int id)
        {
            try
            {
                if (File.Exists(db_location + "ShoppingBaskets.xml"))
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(List<ShoppingCart>));
                    TextReader reader = new StreamReader(db_location + "ShoppingBaskets.xml");
                    IList<ShoppingCart> b = (List<ShoppingCart>)serializer.Deserialize(reader);
                    reader.Close();
                    return b.Where<ShoppingCart>(cart => cart.Id == id).FirstOrDefault<ShoppingCart>();
                }
                else
                    return null;
            }
            catch (Exception ex)
            {
                throw new SalesTaxException("error reading database", ex);
            }
        }
    }
}
