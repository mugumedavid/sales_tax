﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using SalesTax.DataContext;
using SalesTax.Model;
using SalesTax.Model.Exceptions;
using SalesTax.Process;
using log4net;

namespace SalesTax.UI
{
    public class Program
    {
        private static AutoResetEvent autoResetEvent = null;
        private static SalesProcessor processor = null;
        private static readonly ILog log = LogManager.GetLogger(typeof(Program));

        private static void Main(string[] args)
        {
            log.Debug("Initialising..");
            Console.WriteLine("Initialising..");
            processor = new SalesProcessor(SessionManager.Context);
            processor.InitialiseEnvironmentData();
            Console.WriteLine("\nWelcome to Sales Tax Computation. Press Ctrl-C to terminate");
            Console.WriteLine("===========================================================\n");

            //create a worker thread and start it
            Thread workerThread = new Thread(processor.Worker);
            log.Debug("starting worker..");
            workerThread.Start();

            //create a wait event
            //we will signal when <Ctrl-C> is received
            autoResetEvent = new AutoResetEvent(false);
            Console.CancelKeyPress += new ConsoleCancelEventHandler(Console_CancelKeyPress);

            //Main Thread will block here until we signal on receiving <Ctrl-C>
            autoResetEvent.WaitOne();
            workerThread.Join();
            Console.WriteLine("Shutting down....");
            log.Debug("Shutting down....");
            Thread.Sleep(TimeSpan.FromSeconds(3));
        }

        /// <summary>
        /// Handle the cancel event so that we can signal a clean shutdown
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private static void Console_CancelKeyPress(object sender, ConsoleCancelEventArgs e)
        {
            Console.WriteLine("stop requested..");
            log.Debug("stop requested..");
            processor.Stop_requested = true;
            e.Cancel = true;
            autoResetEvent.Set();
        }
    }
}