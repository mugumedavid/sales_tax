﻿using System;
using System.Collections.Generic;
using System.Linq;
using log4net;
using SalesTax.DataAccessService;
using SalesTax.Model;
using SalesTax.Model.Exceptions;
using System.Threading;

namespace SalesTax.Process
{
    public class SalesProcessor
    {
        private readonly IDataAccess context;
        private IList<Tax> taxes;
        private bool stop_requested = false;
        private static readonly ILog log = LogManager.GetLogger(typeof(SalesProcessor));

        public SalesProcessor(IDataAccess context)
        {
            this.context = context;
            taxes = context.GetAllTax();
        }

        /// <summary>
        /// mocking a UI operation by calling the business layer to create and persist data
        /// </summary>
        public void InitialiseEnvironmentData()
        {
            try
            {
                //initialise default taxes in our db if not exist
                if (context.GetAllTax().Count == 0)
                {
                    log.Info("initialising default taxes");
                    SaveTax(1, "basic sales tax", 10F, true, true, false);
                    SaveTax(2, "import duty tax", 5F, true, false, true);
                }

                //initial default products in our db if non exist
                if (context.GetAllProducts().Count == 0)
                {
                    log.Info("initialising default products");
                    SaveProduct(1, "book", 12.49F, false, false);
                    SaveProduct(2, "music CD", 14.99F, true, false);
                    SaveProduct(3, "chocolate bar", 0.85F, false, false);
                    SaveProduct(4, "imported box of chocolates", 10.00F, false, true);
                    SaveProduct(5, "imported bottle of perfume", 47.50F, true, true);
                    SaveProduct(6, "imported bottle of perfume", 27.99F, true, true);
                    SaveProduct(7, "bottle of perfume", 18.99F, true, false);
                    SaveProduct(8, "packet of headache pills", 9.75F, false, false);
                    SaveProduct(9, "box of imported chocolates", 11.25F, false, true);
                }

                //initialise shopping baskets if non exist
                //this mocks a user selecting preferred products, based on the selected products' ids, we create a shopping basket
                if (context.GetAllShoppingCarts().Count == 0)
                {
                    log.Info("initialising default shopping carts");
                    IList<Product> products = context.GetAllProducts();
                    //add products to cart based on ids
                    SaveShoppingBasket(1, products.Where<Product>(p => new int[] { 1, 2, 3 }.Contains(p.Id)).ToList<Product>());
                    SaveShoppingBasket(2, products.Where<Product>(p => new int[] { 4, 5 }.Contains(p.Id)).ToList<Product>());
                    SaveShoppingBasket(3, products.Where<Product>(p => new int[] { 6, 7, 8, 9 }.Contains(p.Id)).ToList<Product>());
                }
            }
            catch (SalesTaxException ex)
            {
                Console.WriteLine(ex.Message);
                log.Error(ex.Message, ex);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error " + ex.Message);
                log.Error(ex.Message, ex);
            }
        }

        /// <summary>
        /// processing requests
        /// </summary>
        public void Worker()
        {
            Console.WriteLine("Please provide the Shopping Basket's Id when requested for Input");
            while (!stop_requested)
            {
                //in GUI a dispatcher would have to be employed to update the UI elements
                Console.WriteLine("\n");
                Console.Write("Input :");
                string input = Console.ReadLine();
                if (!string.IsNullOrWhiteSpace(input))
                {
                    try
                    {
                        Console.Write(DisplayShoppingCartReceipt(input));
                    }
                    catch (SalesTaxException ex)
                    {
                        Console.WriteLine(ex.Message);
                        log.Error(ex.Message, ex);
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("Error " + ex.Message);
                        log.Error(ex.Message, ex);
                    }
                }
                Thread.Sleep(TimeSpan.FromSeconds(1));
            }
        }

        /// <summary>
        /// takes tax details, creates a tax object and persists to database
        /// </summary>
        /// <param name="id"></param>
        /// <param name="tax_name"></param>
        /// <param name="amount"></param>
        /// <param name="amount_is_percent"></param>
        /// <param name="is_basic_tax"></param>
        /// <param name="is_import_tax"></param>
        public void SaveTax(int id, string tax_name, float amount, bool amount_is_percent, bool is_basic_tax, bool is_import_tax)
        {
            Tax tax = new Tax();
            tax.Id = id;
            tax.Name = tax_name;
            tax.Amount = amount;
            tax.IsPercent = amount_is_percent;
            tax.IsBasicTax = is_basic_tax;
            tax.IsImportTax = is_import_tax;
            context.SaveOrUpdateTax(tax);
        }

        /// <summary>
        /// Creates a product model object, populates it and saves to database,
        /// In more advanced implementation, UI elements can bind to this method
        /// </summary>
        /// <param name="id">products id</param>
        /// <param name="name">products name</param>
        /// <param name="price">products price</param>
        /// <param name="attracts_basic_tax">true if products attacts basic tax, else false</param>
        /// <param name="attracts_import_tax">true if product attracts import tax, else false</param>
        public void SaveProduct(int id, string name, float price, bool attracts_basic_tax, bool attracts_import_tax)
        {
            Product product = new Product();
            product.Id = id;
            product.Name = name;
            product.Price = price;
            product.AttractsImportTax = attracts_import_tax;
            product.AttractsBasicTax = attracts_basic_tax;
            product.TotalTax = CalculateSalesTaxes(product);
            product.PriceInclusiveOfTax = price + product.TotalTax;
            context.SaveOrUpdateProduct(product);
        }

        public void SaveShoppingBasket(int id, IList<Product> productsInBasket)
        {
            ShoppingCart cart = new ShoppingCart();
            cart.Id = id;
            cart.Products = productsInBasket.ToList<Product>();//ILists are not serializable
            context.SaveShoppingCart(cart);
        }

        private float CalculateSalesTaxes(Product product)
        {
            float total_tax = 0F;
            if (taxes == null || taxes.Count == 0) taxes = context.GetAllTax();
            foreach (Tax tax in taxes)
            {
                if (product.AttractsBasicTax && tax.IsBasicTax)
                {
                    if (tax.IsPercent)
                        total_tax += tax.Amount * product.Price / 100;
                    else
                        total_tax += tax.Amount;
                }
                else if (product.AttractsImportTax && tax.IsImportTax)
                {
                    if (tax.IsPercent)
                        total_tax += tax.Amount * product.Price / 100;
                    else
                        total_tax += tax.Amount;
                }
            }
            //apply rounding rules for sales tax
            total_tax = (float)Math.Ceiling(total_tax * 20) / 20;
            //we are not dealing with big figures, the type cast shouldn't make us lose precision

            return total_tax;
        }

        /// <summary>
        /// returns a receipt for a shopping basket that has a given id
        /// </summary>
        /// <param name="cartId">this field is from a UI hence typical datatype would be string, the id of the shopping cart whose contents are up for display</param>
        /// <returns>formatted display of contents in a shopping basketed according to how the UI expects it</returns>
        public string DisplayShoppingCartReceipt(string cartId)
        {
            int id;
            string displayContent = "";
            try
            {
                ShoppingCart cart = null;

                if (int.TryParse(cartId, out id))
                    cart = context.GetShoppingCartById(id);

                if (cart != null)
                    displayContent = cart.ToString();
                else
                    displayContent = "<<<<COULD NOT RETRIEVE SHOPPING BASKET>>>>";
            }
            catch (SalesTaxException ex)
            {
                displayContent = ex.Message;
                log.Error(ex.Message, ex);
            }
            catch (Exception ex)
            {
                displayContent = "Error " + ex.Message;
                log.Error(ex.Message, ex);
            }

            return displayContent;
        }

        public IList<Product> GetAllProducts()
        {
            return context.GetAllProducts();
        }

        public IList<Tax> GetAllTax()
        {
            return taxes;
        }

        public IList<ShoppingCart> GetAllShoppingBaskets()
        {
            return context.GetAllShoppingCarts();
        }

        /// <summary>
        /// gets or sets the value specifying whether stop has been requested
        /// </summary>
        public bool Stop_requested
        {
            get { return stop_requested; }
            set { stop_requested = value; }
        }
    }
}
