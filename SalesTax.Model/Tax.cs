﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SalesTax.Model
{
    [Serializable]
    public class Tax
    {
        private int id;
        private string name;
        private float amount;
        private bool isPercent;
        private bool isBasicTax;
        private bool isImportTax;

        public bool IsBasicTax
        {
          get { return isBasicTax; }
          set { isBasicTax = value; }
        }

        public bool IsImportTax
        {
            get { return isImportTax; }
            set { isImportTax = value; }
        }

        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        public bool IsPercent
        {
            get { return isPercent; }
            set { isPercent = value; }
        }

        public float Amount
        {
            get { return amount; }
            set { amount = value; }
        }

        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        public override string ToString()
        {
            return IsPercent ? Amount.ToString("0.00%") : Amount.ToString("0.00");
        }
    }
}
