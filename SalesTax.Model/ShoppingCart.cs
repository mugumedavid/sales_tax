﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SalesTax.Model
{
    [Serializable]
    public class ShoppingCart
    {
        private int id;
        private List<Product> products;

        public List<Product> Products
        {
            get { return products; }
            set { products = value; }
        }

        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        public float TotalTax()
        {
            return products.Sum<Product>(p=>p.TotalTax);
        }

        public float ToTal()
        {
            return products.Sum<Product>(p => p.PriceInclusiveOfTax);
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine();
            sb.AppendLine(string.Format("Output {0}:", Id));
            foreach (Product product in Products)
                sb.AppendLine(product.SaleDisplay());
            sb.AppendLine(string.Format("Sales Taxes: {0}", TotalTax().ToString("0.00")));
            sb.AppendLine(string.Format("Total: {0}",ToTal()));
            return sb.ToString();
        }
    }
}
