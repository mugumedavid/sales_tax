﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SalesTax.Model.Exceptions
{
    public class SalesTaxException: Exception
    {
        public SalesTaxException() : base() { }
        public SalesTaxException(string message) : base(message) { }
        public SalesTaxException(string message, Exception innerException) : base(message, innerException) { }
    }
}
