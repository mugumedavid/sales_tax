﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SalesTax.Model
{
    [Serializable]
    public class Product
    {
        private int id;
        private string name;
        private float price;
        private float priceInclusiveOfTax;
        private bool attractsImportTax;
        private bool attractsBasicTax;
        private float totalTax;

        public float TotalTax
        {
            get { return totalTax; }
            set { totalTax = value; }
        }

        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        public bool AttractsBasicTax
        {
            get { return attractsBasicTax; }
            set { attractsBasicTax = value; }
        }

        public bool AttractsImportTax
        {
            get { return attractsImportTax; }
            set { attractsImportTax = value; }
        }

        public float Price
        {
            get { return price; }
            set { price = value; }
        }

        public float PriceInclusiveOfTax
        {
            get { return priceInclusiveOfTax; }
            set { priceInclusiveOfTax = value; }
        }

        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        public string SaleDisplay()
        {
            return string.Format("1 {0}: {1}", Name, PriceInclusiveOfTax.ToString("0.00"));
        }

        public override string ToString()
        {
            return string.Format("{0} at {1}", Name, Price.ToString("0.00"));
        }
    }
}
