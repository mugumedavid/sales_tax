﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SalesTax.Model;

namespace SalesTax.DataAccessService
{
    /// <summary>
    /// with this interface, we can have separation of modules
    /// </summary>
    public interface IDataAccess
    {
        /// <summary>
        /// save or updates a product to database
        /// </summary>
        /// <param name="product">Product to be persisted</param>
        void SaveOrUpdateProduct(Product product);

        /// <summary>
        /// save or updates a tax to database
        /// </summary>
        /// <param name="tax">the tax to be persisted</param>
        void SaveOrUpdateTax(Tax tax);

        /// <summary>
        /// saves a shopping basket to database
        /// </summary>
        /// <param name="cart">The shoppig basket to be persisted</param>
        void SaveShoppingCart(ShoppingCart cart);

        /// <summary>
        /// returns all tax items
        /// </summary>
        /// <returns></returns>
        IList<Tax> GetAllTax();

        /// <summary>
        /// returns all saved shopping baskets
        /// </summary>
        /// <returns></returns>
        IList<ShoppingCart> GetAllShoppingCarts();

        /// <summary>
        /// returns all products available for sale
        /// </summary>
        /// <returns></returns>
        IList<Product> GetAllProducts();

        /// <summary>
        /// returns a shopping basket with a given id
        /// </summary>
        /// <param name="id">the id of the shopping basket to be returned</param>
        /// <returns></returns>
        ShoppingCart GetShoppingCartById(int id);
    }
}
